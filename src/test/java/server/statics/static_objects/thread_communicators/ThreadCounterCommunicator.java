package server.statics.static_objects.thread_communicators;

public class ThreadCounterCommunicator {
    private static ThreadCounterCommunicator threadCounterCommunicator;

    private int counter;
    private String notifyMessage;
    private boolean assertStatus;
    private String notified;

    private ThreadCounterCommunicator(){
        counter = 0;
    }
    public static ThreadCounterCommunicator getThreadCounterCommunicator(){
        if (threadCounterCommunicator == null){
            threadCounterCommunicator = new ThreadCounterCommunicator();
        }
        return threadCounterCommunicator;
    }

    public void increase(){
        ++counter;
    }

    public int getCounter() {
        return counter;
    }

    public void finish(){
        threadCounterCommunicator = null;
    }

    public String getNotifyMessage() {
        return notifyMessage;
    }

    public void setNotifyMessage(String notifyMessage) {
        this.notifyMessage = notifyMessage;
    }

    public String getNotified() {
        return notified;
    }

    public void setNotified(String notified) {
        this.notified = notified;
    }

    public boolean isAssertStatus() {
        return assertStatus;
    }

    public void setAssertStatus(boolean assertStatus) {
        this.assertStatus = assertStatus;
    }
}
