package server.test_prerequisites.notification_classes.pc_controller_notification;


import pc_controller.controller.PcController;
import server.functionality_test.pc_controller_test.PcControllerTest;
import server.statics.static_objects.thread_communicators.ThreadCounterCommunicator;
import socket_installer.SI_behavior.abstractClasses.notification.data_trade.DataTrade;
import socket_installer.SI_behavior.abstractClasses.notification.notification_state_exceptions.NotificationerStatesBundle;
import socket_installer.SI_behavior.abstractClasses.sockets.socket.client.ClientSocket;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.class_annotation.class_identifier.ClassIdentifier;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.methods_annotation.method_identifier.MethodIdentifier;


@ClassIdentifier(identification = "pcControllerIdent")
public class PCcontrollerNotifcationTest extends DataTrade {
    private final String IDENT = "pcControllerIdent";

    @MethodIdentifier(identification = "pcControllerIdent_takeScreenShotRequest")
    public void takeScreenShotRequest(String notification,NotificationerStatesBundle notificationerStatesBundle){
        PcController pcController = (PcController) getExternalContext().getContextObject("PcController").getObject();
        pcController.getScreenCapturer().takeScreenShotTest();
        ThreadCounterCommunicator.getThreadCounterCommunicator().setNotified(PcControllerTest.checkIfCurrentFileModelIsRootNotify);
    }

    @Override
    public boolean exceptionHandler(ClientSocket clientSocket, NotificationerStatesBundle notificationerStatesBundle) {
        return false;
    }
}
