package server.test_prerequisites.notification_classes.files_notification;


import file_manager.manager.FileManager;
import server.functionality_test.file_explorer_test.android_communication.AndroidExplorerTest;
import server.functionality_test.file_explorer_test.internal_tests.FileExplorerTest;
import server.statics.static_objects.thread_communicators.ThreadCounterCommunicator;
import socket_installer.SI_behavior.abstractClasses.notification.data_trade.DataTrade;
import socket_installer.SI_behavior.abstractClasses.notification.notification_state_exceptions.NotificationerStatesBundle;
import socket_installer.SI_behavior.abstractClasses.sockets.socket.client.ClientSocket;
import socket_installer.SI_behavior.abstractClasses.sockets.socket_managers.error_manager.exceptions.SocketExceptions;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.class_annotation.class_identifier.ClassIdentifier;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.methods_annotation.method_identifier.MethodIdentifier;

import java.io.IOException;

@ClassIdentifier(identification = "fileExplorerIdent")
public class FileExplorerNotificationsMethodsTest extends DataTrade {
    private final String IDENT = "fileExplorerIdent";

    @MethodIdentifier(identification = "fileExplorerIdent_currentActiveFileRequest")
    public void getCurrentActiveFileModel(String notification, NotificationerStatesBundle notificationerStatesBundle) throws IOException, SocketExceptions {
        FileManager fileManager = (FileManager) getExternalContext().getContextObject("FileManager").getObject();
        send(IDENT,"fileExplorerIdent_currentActiveFileResponse",fileManager.getActiveFolderModel().getAllFilesAndFolders()[0].getFileName());
        ThreadCounterCommunicator.getThreadCounterCommunicator().setNotified(FileExplorerTest.sendCurrentActiveFile);
    }



    @MethodIdentifier(identification = "fileExplorerIdent_getFilesAndFoldersFromAndroid")
    public void getFilesAndFoldersFromAndroid(String notification, NotificationerStatesBundle notificationerStatesBundle) throws IOException, SocketExceptions {
        System.out.println(notification);
        send("generalTestIdent","generalTestIdent_checkStatus","success");
        ThreadCounterCommunicator.getThreadCounterCommunicator().setNotified(AndroidExplorerTest.getFilesAndFoldersFromAndroid);
    }


    @Override
    public boolean exceptionHandler(ClientSocket clientSocket, NotificationerStatesBundle notificationerStatesBundle) {
        return false;
    }
}
