package server.test_prerequisites.notification_classes.user_notification;

import server.functionality_test.basic_test.BasicTestFunctionality;
import server.statics.static_objects.thread_communicators.ThreadCounterCommunicator;
import socket_installer.SI_behavior.abstractClasses.notification.data_trade.DataTrade;
import socket_installer.SI_behavior.abstractClasses.notification.notification_state_exceptions.NotificationerStatesBundle;
import socket_installer.SI_behavior.abstractClasses.sockets.socket.client.ClientSocket;
import socket_installer.SI_behavior.abstractClasses.sockets.socket_managers.error_manager.exceptions.SocketExceptions;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.class_annotation.class_identifier.ClassIdentifier;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.methods_annotation.method_identifier.MethodIdentifier;

import java.io.IOException;

import static org.assertj.core.api.Assertions.*;

@ClassIdentifier(identification = "userIdentTest")
public class UserNotificationMethodsTest extends DataTrade {


    @MethodIdentifier(identification = "userIdent_loginAppRequest")
    public void userIdentLoginAppRequest(String loginCredentials,NotificationerStatesBundle notificationerStatesBundle) throws IOException, SocketExceptions {
        System.out.println("Login credentials : "+loginCredentials);
        assertThat(loginCredentials).isEqualTo("Login credentials :{username: Filip,password:sifra}");
        send("userIdentTest","userIdent_loginAppResponse","success login");

        ThreadCounterCommunicator.getThreadCounterCommunicator().setNotified(BasicTestFunctionality.checkIfServerCanReceiveDataNotify);
    }


    @Override
    public boolean exceptionHandler(ClientSocket clientSocket,NotificationerStatesBundle notificationerStatesBundle) {
        return false;
    }

}
