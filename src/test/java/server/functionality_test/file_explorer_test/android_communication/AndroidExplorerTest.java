package server.functionality_test.file_explorer_test.android_communication;

import org.junit.Rule;
import org.junit.Test;
import server.tools.rules.ServerResource;

public class AndroidExplorerTest {
    public static final String CLASS_IDENT = "fileExplorerIdent";

    @Rule
    public ServerResource serverResource = new ServerResource();

    public static String getFilesAndFoldersFromAndroid = "getFilesAndFoldersFromAndroid";
    @Test
    public void checkIfFilesAndFoldersFromAndroidAreSentToServer(){
        serverResource.setThreadCounterCommunicatorNotify(getFilesAndFoldersFromAndroid);
        serverResource.waitForThreadCounterToPass(getFilesAndFoldersFromAndroid);
    }


}
