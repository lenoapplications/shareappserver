package server.functionality_test.file_explorer_test.internal_tests;

import org.junit.Rule;
import org.junit.Test;
import server.tools.rules.ServerResource;

public class FileExplorerTest {
    public static final String CLASS_IDENT = "fileExplorerIdent";

    @Rule
    public ServerResource serverResource = new ServerResource();

    public static String sendCurrentActiveFile = "sendCurrentActiveFile";
    @Test
    public void sendCurrentActiveFileAndCheckIfItIsSend(){
        serverResource.setThreadCounterCommunicatorNotify(sendCurrentActiveFile);
        serverResource.waitForThreadCounterToPass(sendCurrentActiveFile);
    }

}
