package server.functionality_test.pc_controller_test;


import org.junit.Rule;
import org.junit.Test;
import server.tools.rules.ServerResource;

public class PcControllerTest {
    public static final String CLASS_IDENT = "pcControllerTest";

    @Rule
    public ServerResource server = new ServerResource();

    public static final String checkIfCurrentFileModelIsRootNotify ="checkIfScreenShotIsTaken";
    @Test
    public void checkIfScreenShotWasTaken(){
       server.setThreadCounterCommunicatorNotify(checkIfCurrentFileModelIsRootNotify);
       server.waitForThreadCounterToPass(checkIfCurrentFileModelIsRootNotify);
    }
}
