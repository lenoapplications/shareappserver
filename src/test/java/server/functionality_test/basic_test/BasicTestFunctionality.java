package server.functionality_test.basic_test;

import org.junit.Rule;
import org.junit.Test;
import server.tools.rules.ServerResource;

public class BasicTestFunctionality {

    @Rule
    public ServerResource server = new ServerResource();


    public final static String checkIfServerCanReceiveDataNotify = "checkIfServerCanRecvData";
    @Test
    public void checkIfServerCanRecieveData() throws InterruptedException {
        server.setThreadCounterCommunicatorNotify(checkIfServerCanReceiveDataNotify);
        server.waitForThreadCounterToPass(checkIfServerCanReceiveDataNotify);
    }

    @Test
    public void checkIfClientIsProperlyClosed(){

    }
}
