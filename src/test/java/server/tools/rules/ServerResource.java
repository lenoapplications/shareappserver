package server.tools.rules;

import org.junit.rules.ExternalResource;
import server.app.service_layer.notification_classes.connection_notification.ConnectionNotification;
import server.app.service_layer.notification_classes.files_notifications.FileTransferNotificationPc;
import server.test_prerequisites.notification_classes.files_notification.FileExplorerNotificationsMethodsTest;
import server.test_prerequisites.notification_classes.pc_controller_notification.PCcontrollerNotifcationTest;
import server.test_prerequisites.notification_classes.user_notification.UserNotificationMethodsTest;
import server.components.components_configuration.socket_installer_config.server.Server;
import server.components.components_configuration.socket_installer_config.server.external_context.ExternalContextInitializator;
import server.components.components_configuration.socket_installer_config.server.notificationer.ServerNotificationer;
import server.statics.static_objects.thread_communicators.ThreadCounterCommunicator;
import socket_installer.SI_behavior.interfaces.notification.DataTradeModel;

import static server.statics.static_fields.StaticFields.PORT;
import static server.statics.static_fields.StaticFields.SERVER_HOST;
import static server.statics.static_fields.StaticFields.TIMEOUT;
import static server.statics.static_methods.StaticMethods.*;

public class ServerResource extends ExternalResource {
    private Server server;
    private ThreadCounterCommunicator threadCounterCommunicator;

    @Override
    protected void before() throws Throwable {
        server = new Server();
        server.configureSocketTest(SERVER_HOST,PORT,TIMEOUT,2);

        threadCounterCommunicator = ThreadCounterCommunicator.getThreadCounterCommunicator();
        server.setServerOnline();
    }

    public void setThreadCounterCommunicatorNotify(String notify){
        threadCounterCommunicator.setNotifyMessage(notify);
        threadCounterCommunicator.setNotified("not notified");
    }

    public void waitForThreadCounterToPass(String waitForNotify){
        while(true){
            sleep(5);
            if ( threadCounterCommunicator.getNotified().equals(waitForNotify)){
                break;
            }
        }
    }
}
