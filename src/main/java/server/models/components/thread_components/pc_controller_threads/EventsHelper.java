package server.models.components.thread_components.pc_controller_threads;

public abstract class EventsHelper {

    protected float[] screenDifference;

    protected EventsHelper() {
        screenDifference = new float[2];
    }
    public void initScreenDifference(float x,float y){
        screenDifference[0] = x;
        screenDifference[1] = y;
    }

    protected int caluclateXDifference(int xCoordinate){
        return (int) (xCoordinate * screenDifference[0]);
    }
    protected int caluclateYDifference(int yCoordinate){
        return (int) (yCoordinate * screenDifference[1]);
    }
    protected int parseXCoordinateFromString(String event){
        int seperatorIndex = event.indexOf("|");
        return Integer.parseInt(event.substring(event.indexOf("(")+1,seperatorIndex));
    }
    protected int parseYCoordinateFromString(String event){
        int seperatorIndex = event.indexOf("|");
        return Integer.parseInt(event.substring(seperatorIndex+1,event.indexOf(")")));
    }

}
