package server.app.service_layer.notification_classes.connection_notification;


import protocol.statics.class_fixed_messages.ConnectionFixedMessages;
import protocol.statics.class_notification_protocol.ConnectionNotificationProtocol;
import socket_installer.SI_behavior.abstractClasses.notification.data_trade.DataTrade;
import socket_installer.SI_behavior.abstractClasses.notification.notification_state_exceptions.NotificationerStatesBundle;
import socket_installer.SI_behavior.abstractClasses.sockets.socket.client.ClientSocket;
import socket_installer.SI_behavior.abstractClasses.sockets.socket_managers.error_manager.exceptions.SocketExceptions;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.class_annotation.class_identifier.ClassIdentifier;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.methods_annotation.method_identifier.MethodIdentifier;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.methods_annotation.method_identifier.StreamOpen;

import java.io.IOException;

@ClassIdentifier(identification = ConnectionNotificationProtocol.CLASS_IDENT)
public class ConnectionNotification extends DataTrade {


    @MethodIdentifier(identification = ConnectionNotificationProtocol.CHECK_IF_SERVER_ONLINE_REQUEST,closeStream = true)
    public void checkIfServerOnline(String notification, NotificationerStatesBundle notificationerStatesBundle) throws IOException, SocketExceptions {
        send(ConnectionNotificationProtocol.CLASS_IDENT,
                ConnectionNotificationProtocol.CHECK_IF_SERVER_ONLINE_RESPONSE,
                ConnectionFixedMessages.SERVER_ONLINE);
    }

    @MethodIdentifier(identification = ConnectionNotificationProtocol.CONNECT_TO_SERVER_REQUEST)
    @StreamOpen
    public void connectToServerRequest(String notification, NotificationerStatesBundle notificationerStatesBundle) throws IOException, SocketExceptions {
        switch (notification){
            case ConnectionFixedMessages.CONNECT_REQUEST :{
                send(ConnectionNotificationProtocol.CLASS_IDENT,
                        ConnectionNotificationProtocol.CONNECT_TO_SERVER_RESPONSE,
                        ConnectionFixedMessages.CONNECTION_APPROVED);
                closeStream();
                break;
            }
        }
    }

    @MethodIdentifier(identification = ConnectionNotificationProtocol.DISCONNECT_FROM_SERVER_REQUEST,closeStream = true)
    public void disconnectFromServerRequest(String notification,NotificationerStatesBundle notificationerStatesBundle){
        resetExternalContext();
        disconnectFromServer();
        System.out.println("GASIM SERVER IZ TOGA");
    }

    @Override
    public boolean exceptionHandler(ClientSocket clientSocket, NotificationerStatesBundle notificationerStatesBundle) {
        return false;
    }
}
