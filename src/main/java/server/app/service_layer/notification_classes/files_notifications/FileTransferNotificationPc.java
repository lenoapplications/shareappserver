package server.app.service_layer.notification_classes.files_notifications;


import file_manager.manager.FileManager;
import protocol.statics.class_fixed_messages.FileTransferFixedMessages;
import protocol.statics.class_fixed_messages.PcControllerFixedMessages;
import protocol.statics.class_notification_protocol.FileTransferNotificationProtocol;
import socket_installer.SI_behavior.abstractClasses.notification.data_trade.DataTrade;
import socket_installer.SI_behavior.abstractClasses.notification.notification_state_exceptions.NotificationerStatesBundle;
import socket_installer.SI_behavior.abstractClasses.sockets.socket.client.ClientSocket;
import socket_installer.SI_behavior.abstractClasses.sockets.socket_managers.error_manager.exceptions.SocketExceptions;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.class_annotation.class_identifier.ClassIdentifier;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.methods_annotation.method_identifier.MethodIdentifier;
import socket_installer.SI_parts.protocol.enum_protocols.general_protocols.SignalProtocol;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static protocol.statics.class_fixed_messages.FileTransferFixedMessages.FOLDER_EMPTY;
import static protocol.statics.class_notification_protocol.FileTransferNotificationProtocol.*;

@ClassIdentifier(identification = CLASS_IDENT_PC)
public class FileTransferNotificationPc extends DataTrade {


    @MethodIdentifier(identification = FileTransferNotificationProtocol.CONNECT_TO_PC_STORAGE_REQUEST,closeStream = true)
    public void connectToPcStorage(String notification, NotificationerStatesBundle notificationerStatesBundle) throws IOException, SocketExceptions {
        FileManager fileManager = (FileManager) getExternalContext().getContextObject("FileManager").getObject();
        String root = fileManager.currentFilesAndFoldersToString();
        send(CLASS_IDENT_PC,CONNECT_TO_PC_STORAGE_RESPONSE,root);
    }


    @MethodIdentifier(identification = FileTransferNotificationProtocol.PC_STORAGE_OPEN_FOLDER_REQUEST,closeStream = true)
    public void openFolder(String notification, NotificationerStatesBundle notificationerStatesBundle) throws IOException, SocketExceptions {
        FileManager fileManager = (FileManager) getExternalContext().getContextObject("FileManager").getObject();
        String folderToOpen = notification.substring(FileTransferFixedMessages.OPEN_FOLDER_BEGIN_INDEX);
        try {
            fileManager.clickedOnFileModel(folderToOpen);
            String childernOfFolderOpend = fileManager.currentFilesAndFoldersToString();
            System.out.println("OPEN FOLDER  "+childernOfFolderOpend);
            if (childernOfFolderOpend.length() == 0){
                childernOfFolderOpend = FOLDER_EMPTY;
            }

            send(CLASS_IDENT_PC,PC_STORAGE_OPEN_FOLDER_RESPONSE,childernOfFolderOpend);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @MethodIdentifier(identification = FileTransferNotificationProtocol.PC_STORAGE_CLOSE_FOLDER_REQUEST,closeStream = true)
    public void closeFolder(String notification, NotificationerStatesBundle notificationerStatesBundle) throws IOException, SocketExceptions {
        FileManager fileManager = (FileManager) getExternalContext().getContextObject("FileManager").getObject();
        fileManager.goBackToParentFileModel();

        String nameOfCurrentFolder = fileManager.getActiveFolderModel().getFileInformationHolder().getStringFileRepresenter().getCleanFileName();
        String childrenOfFolderOpend = fileManager.currentFilesAndFoldersToString();
        String completeString = String.format(FileTransferFixedMessages.CLOSE_FOLDER,childrenOfFolderOpend,nameOfCurrentFolder);

        send(CLASS_IDENT_PC,PC_STORAGE_CLOSE_FOLDER_RESPONSE,completeString);
    }


    @MethodIdentifier(identification = FileTransferNotificationProtocol.PC_STORAGE_SHOW_FILE_PROPERTIES_REQUEST,closeStream = true)
    public void showFileProperties(String notification,NotificationerStatesBundle notificationerStatesBundle) throws IOException, SocketExceptions {
        FileManager fileManager = (FileManager) getExternalContext().getContextObject("FileManager").getObject();
        String properties = fileManager.getFileProperties(notification);
        send(CLASS_IDENT_PC,PC_STORAGE_SHOW_FILE_PROPERTIES_RESPONSE,properties);
    }

    @MethodIdentifier(identification = FileTransferNotificationProtocol.PC_STORAGE_SHOW_FILE_CONTENT_REQUEST,closeStream = true)
    public void showFileContent(String notification, NotificationerStatesBundle notificationerStatesBundle) throws IOException, SocketExceptions {
        FileManager fileManager = (FileManager) getExternalContext().getContextObject("FileManager").getObject();
        String fileType = fileManager.checkFileType(notification);
        System.out.println(fileType);
        send(CLASS_IDENT_PC,PC_STORAGE_SHOW_FILE_CONTENT_RESPONSE,fileType);

        if (!fileType.equals("unknown")){
            waitForSignal();
            File file = fileManager.getFileObjectFromCurrentActiveFolder(notification);
            if (file != null){
                System.out.println("reading file");
                FileInputStream fileInputStream = new FileInputStream(file);
                byte[] bytes = new byte[(int) file.length()];
                fileInputStream.read(bytes);
                sendSizeOfDownload(bytes.length);
                System.out.println(bytes.length);
                System.out.println("sent file size");
                waitForSignal();
                System.out.println("sent file bytes");
                upload(bytes);
                waitForSignal();
            }else{
                sendSignal(SignalProtocol.SIGNAL_PROBLEM_OCCURED.getProtocol());
            }
        }
    }




    @Override
    public boolean exceptionHandler(ClientSocket clientSocket, NotificationerStatesBundle notificationerStatesBundle) {
        return false;
    }
}
