package server.app.service_layer.notification_classes.user_notifications;

import socket_installer.SI_behavior.abstractClasses.notification.data_trade.DataTrade;
import socket_installer.SI_behavior.abstractClasses.sockets.socket.client.ClientSocket;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.class_annotation.class_identifier.ClassIdentifier;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.methods_annotation.method_identifier.MethodIdentifier;
import socket_installer.SI_behavior.abstractClasses.notification.notification_state_exceptions.NotificationerStatesBundle;


@ClassIdentifier(identification = "userIdent")
public class UserNotificationMethods extends DataTrade {

    @MethodIdentifier(identification = "userIdent_loginAppRequest")
    public void userIdentLoginAppRequest(String loginCredentials,NotificationerStatesBundle notificationerStatesBundle){
        System.out.println("Login credentials : "+loginCredentials);
    }


    @Override
    public boolean exceptionHandler(ClientSocket clientSocket,NotificationerStatesBundle notificationerStatesBundle) {
        return false;
    }
}
