package server.app.service_layer.notification_classes.pc_controller;

import pc_controller.controller.PcController;
import protocol.statics.class_notification_protocol.PcControllerNotificationProtocol;
import server.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.pc_controller_threads.BufferedImageConvertThread;
import server.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.pc_controller_threads.event_handler_threads.EventHandlerThread;
import socket_installer.SI_behavior.abstractClasses.notification.data_trade.DataTrade;
import socket_installer.SI_behavior.abstractClasses.notification.notification_state_exceptions.NotificationerStatesBundle;
import socket_installer.SI_behavior.abstractClasses.sockets.socket.client.ClientSocket;
import socket_installer.SI_behavior.abstractClasses.sockets.socket_managers.error_manager.exceptions.SocketExceptions;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.class_annotation.class_identifier.ClassIdentifier;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.methods_annotation.method_identifier.MethodIdentifier;
import socket_installer.SI_behavior.annotations.user_implementation.methods_implementation.methods_annotation.method_identifier.StreamOpen;
import socket_installer.SI_behavior.interfaces.notification.SignalListenerModel;
import thread_watcher.thread.caller.ThreadCaller;

import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;

import static protocol.statics.class_fixed_messages.PcControllerFixedMessages.START_PC_SCREEN_VIEW;
import static protocol.statics.class_notification_protocol.PcControllerNotificationProtocol.CLASS_IDENT;
import static protocol.statics.class_notification_protocol.PcControllerNotificationProtocol.SHOW_CURRENT_PC_SCREEN_RESPONSE;

@ClassIdentifier(identification = CLASS_IDENT)
public class PcControllerNotification extends DataTrade {

    @MethodIdentifier(identification = PcControllerNotificationProtocol.SHOW_CURRENT_PC_SCREEN_REQUEST)
    @StreamOpen
    public void showCurrentPcScreen(String notification,NotificationerStatesBundle notificationerStatesBundle) throws IOException, SocketExceptions, IllegalAccessException, InstantiationException {

        if (notification.contains(START_PC_SCREEN_VIEW.substring(0,START_PC_SCREEN_VIEW.indexOf("(")))){
            send(CLASS_IDENT,SHOW_CURRENT_PC_SCREEN_RESPONSE, START_PC_SCREEN_VIEW);
            streamActivated(notification.substring(notification.indexOf("(")+1,notification.indexOf(")")));
        }
    }
    private void streamActivated(String screenSize) throws IOException, SocketExceptions, InstantiationException, IllegalAccessException {
        ConcurrentLinkedQueue<byte[]> screenShots = preSetupBufferedImageConvertThread();
        ConcurrentLinkedQueue<String> events = preSetupHandleEventsThread(screenSize);
        uploadScreenShots(screenShots,events);
    }
    private ConcurrentLinkedQueue<byte[]> preSetupBufferedImageConvertThread() throws IllegalAccessException, InstantiationException {
        PcController pcController = (PcController) getExternalContext().getContextObject("PcController").getObject();
        ConcurrentLinkedQueue<byte[]> screenShots = new ConcurrentLinkedQueue<>();
        ThreadCaller.getThreadCaller().callThread(BufferedImageConvertThread.class,"captureScreen",pcController,screenShots);
        return screenShots;
    }
    private ConcurrentLinkedQueue<String> preSetupHandleEventsThread(String screenSize) throws IllegalAccessException, InstantiationException {
        PcController pcController = (PcController) getExternalContext().getContextObject("PcController").getObject();
        ConcurrentLinkedQueue<String> events = new ConcurrentLinkedQueue<>();
        ThreadCaller.getThreadCaller().callThread(EventHandlerThread.class,"handleEvents",pcController,events,screenSize);
        return events;
    }

    private void uploadScreenShots(ConcurrentLinkedQueue<byte[]> screenShots,ConcurrentLinkedQueue<String> events) throws IOException, SocketExceptions {
        char signal = waitForSignal();
        String longSignal;
        byte[] signalBuffer = new byte[25];
        while(signal == '@'){
            if (screenShots.size() > 0){
                byte[] screenShot = screenShots.poll();
                sendSizeOfDownload(screenShot.length);
                waitForSignal();
                upload(screenShot);

                longSignal= waitForLongSignal(signalBuffer);
                signal = longSignal.charAt(0);
                if (longSignal.length() > 1){
                    events.offer(longSignal);
                }


            }
        }
    }


    @Override
    public boolean exceptionHandler(ClientSocket clientSocket, NotificationerStatesBundle notificationerStatesBundle) {
        return false;
    }
}
