package server.components.components_configuration.socket_installer_config.server;

import async_communicator.AsyncCommunicator;
import server.app.service_layer.notification_classes.connection_notification.ConnectionNotification;
import server.app.service_layer.notification_classes.files_notifications.FileTransferNotificationPc;
//import server.app.service_layer.notification_classes.pc_controller.PcControllerNotification;
import server.app.service_layer.notification_classes.pc_controller.PcControllerNotification;
import server.app.service_layer.notification_classes.user_notifications.UserNotificationMethods;
import server.components.components_configuration.socket_installer_config.server.server_socket.ServerSocket;
import server.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.connection_threads.SocketConnectionThreads;
import socket_installer.SI_behavior.interfaces.notification.DataTradeModel;

import thread_watcher.thread.caller.ThreadCaller;


public class Server {
    private ServerSocket serverSocket;
    private final AsyncCommunicator asyncCommunicator;

    public Server(){
        asyncCommunicator = AsyncCommunicator.getAsyncCommunicator();
    }

    public void configureSocket(String host, int port, int timeout, int backlog){
        serverSocket = new ServerSocket(host,port,timeout,backlog,setupDataTradeModels());

    }
    public void configureSocketTest(String host, int port, int timeout, int backlog){
        serverSocket = new ServerSocket(host,port,timeout,backlog,setupDataTradeModels());

    }
    public void setServerOnline(){
        try{
            ThreadCaller.getThreadCaller().callThread(SocketConnectionThreads.class,"setServerOnlineThread",serverSocket.getServerCreatedSocket());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public boolean isServerOnline(){
        return serverSocket.getServerCreatedSocket().isServerRunning();
    }


    private DataTradeModel[] setupDataTradeModels(){
        return new DataTradeModel[]{
                new UserNotificationMethods(),
                new FileTransferNotificationPc(),
                new ConnectionNotification(),
                new PcControllerNotification()
        };
    }

}
