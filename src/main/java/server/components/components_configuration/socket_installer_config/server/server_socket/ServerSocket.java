package server.components.components_configuration.socket_installer_config.server.server_socket;

import server.components.components_configuration.socket_installer_config.server.notificationer.ServerNotificationer;
import socket_installer.SI.socket_creation.server.ServerSocketCreator;
import socket_installer.SI_behavior.abstractClasses.sockets.created_socket.server.ServerCreatedSocket;
import socket_installer.SI_behavior.interfaces.notification.DataTradeModel;

public class ServerSocket {

    private final ServerCreatedSocket serverCreatedSocket;

    public ServerSocket(String host, int port, int timeout, int backlog, DataTradeModel[] dataTrades){
        serverCreatedSocket = ServerSocketCreator.createServer(host,ServerNotificationer.class,dataTrades,port,backlog,timeout);
    }

    public ServerCreatedSocket getServerCreatedSocket() {
        return serverCreatedSocket;
    }
}
