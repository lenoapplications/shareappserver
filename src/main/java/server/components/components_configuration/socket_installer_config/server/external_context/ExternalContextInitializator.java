package server.components.components_configuration.socket_installer_config.server.external_context;

import file_manager.manager.FileManager;
import file_manager.object_forms.models.user_implementation_models.root.RootInitializationModel;
import pc_controller.controller.PcController;
import server.components.components_configuration.statics.screen_capture_config.ScreenCaptureConfig;
import socket_installer.SI_context.context_object.ContextObject;
import socket_installer.SI_context.external_context.ExternalContext;

import java.io.File;

public class ExternalContextInitializator implements socket_installer.SI_behavior.interfaces.context.ExternalContextInitializator {

    PcController pcController = new PcController(ScreenCaptureConfig.LOCATION_PATH,ScreenCaptureConfig.FILE_FORMAT,ScreenCaptureConfig.FILE_NAME_FORMAT);

    @Override
    public void initializeExternalContext(ExternalContext externalContext) {
        try {
            externalContext.saveContextObject(createFileManagerContextObject());
            externalContext.saveContextObject(createPcControllerContextObject());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private ContextObject createFileManagerContextObject() throws Exception {
        FileManager fileManager = new FileManager();
        fileManager.initFileManager(new RootInitializationModel() {
            @Override
            public File[] initializationOfRootFiles() {
                return File.listRoots();
            }

            @Override
            public String getRootIdentification() {
                return null;
            }
        });
        ContextObject contextObject = new ContextObject();
        contextObject.setContextObject(fileManager);
        return contextObject;
    }

    private ContextObject createPcControllerContextObject(){
        ContextObject contextObject = new ContextObject();
        contextObject.setContextObject(pcController);
        return contextObject;
    }
}
