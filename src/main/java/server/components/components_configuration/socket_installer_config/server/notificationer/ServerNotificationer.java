package server.components.components_configuration.socket_installer_config.server.notificationer;

import server.components.components_configuration.socket_installer_config.server.external_context.ExternalContextInitializator;
import socket_installer.SI_behavior.abstractClasses.notification.notificationer_actions.NotificationerActions;
import socket_installer.SI_behavior.abstractClasses.notification.notificationer_actions.connected_client_notificationer_actions.ConnectedClientNotificationerActions;
import socket_installer.SI_behavior.interfaces.notification.DataTradeModel;

public class ServerNotificationer extends ConnectedClientNotificationerActions {
    public ServerNotificationer(DataTradeModel[] objects) {
        super(objects);
        setupExternalContextInitializator(new ExternalContextInitializator());
    }
}
