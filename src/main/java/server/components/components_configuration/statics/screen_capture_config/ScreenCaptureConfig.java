package server.components.components_configuration.statics.screen_capture_config;

public class ScreenCaptureConfig {
    public static final String LOCATION_PATH = "./../data/screen_shots/";
    public static final String FILE_FORMAT = "jpg";
    public static final String FILE_NAME_FORMAT = "screen_shot_%s.%s";
}
