package server.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.pc_controller_threads;

import pc_controller.controller.PcController;
import pc_controller.output_objects.screen_capturer.ScreenCapturer;
import thread_watcher.models.abstractions.user_method.UserMethod;
import thread_watcher.models.annotations.ThreadMethod;
import thread_watcher.thread.caller.ThreadCaller;
import thread_watcher.user_parts.thread_bundle.Bundle;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BufferedImageConvertThread extends UserMethod {

    private final String pcController = "pcCntrll";
    private final String screenShotsTaken ="scrShtTk";

    @ThreadMethod(paramNames = {pcController,screenShotsTaken})
    public void captureScreen(Bundle bundle) throws InstantiationException, IllegalAccessException {
        PcController pc = (PcController) bundle.getArguments(pcController);
        ConcurrentLinkedQueue<byte[]> screenShots = (ConcurrentLinkedQueue<byte[]>) bundle.getArguments(screenShotsTaken);
        ConcurrentLinkedQueue<BufferedImage> bufferedImages = new ConcurrentLinkedQueue<>();
        ThreadCaller.getThreadCaller().callThread(ScreenCapturingThread.class,"screenShooting",pc,bufferedImages);
        takeScreenShots(pc,screenShots,bufferedImages);
    }


    private void takeScreenShots(PcController pcController,ConcurrentLinkedQueue<byte[]> screenShots,ConcurrentLinkedQueue<BufferedImage> bufferedImages){
        ScreenCapturer screenCapturer = pcController.getScreenCapturer();
        while(true){
            if (bufferedImages.size() > 0  ){
                screenShots.offer(screenCapturer.getByteScreenShot(bufferedImages.poll()));
            }
        }
        
    }
}
