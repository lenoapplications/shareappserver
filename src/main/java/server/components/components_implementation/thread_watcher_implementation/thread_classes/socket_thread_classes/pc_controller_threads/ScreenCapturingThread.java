package server.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.pc_controller_threads;

import pc_controller.controller.PcController;
import pc_controller.output_objects.screen_capturer.ScreenCapturer;
import thread_watcher.models.abstractions.user_method.UserMethod;
import thread_watcher.models.annotations.ThreadMethod;
import thread_watcher.user_parts.thread_bundle.Bundle;

import java.awt.image.BufferedImage;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ScreenCapturingThread extends UserMethod {

    private final String bufferedImagesStr = "BfrdImgs";
    private final String pcController = "pcCntrll";

    @ThreadMethod(paramNames = {pcController, bufferedImagesStr})
    public void screenShooting(Bundle bundle) {
        PcController pc = (PcController) bundle.getArguments(this.pcController);
        ConcurrentLinkedQueue<BufferedImage> bufferedImages = (ConcurrentLinkedQueue<BufferedImage>) bundle.getArguments(this.bufferedImagesStr);
        ScreenCapturer screenCapturer = pc.getScreenCapturer();
        while (true) {
            if (bufferedImages.size() < 10){
                bufferedImages.offer(screenCapturer.getScreenShot());
            }
        }
    }
}
