package server.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.connection_threads;

import socket_installer.SI_behavior.abstractClasses.sockets.created_socket.server.ServerCreatedSocket;
import socket_installer.SI_behavior.abstractClasses.sockets.socket_managers.error_manager.exceptions.SocketExceptions;
import thread_watcher.models.abstractions.user_method.UserMethod;
import thread_watcher.models.annotations.ThreadMethod;
import thread_watcher.user_parts.thread_bundle.Bundle;

import java.io.IOException;

public class SocketConnectionThreads extends UserMethod {
    private final String serverCreatedSocketParam = "serverCreatedSocketParam";

    @ThreadMethod(paramNames = {serverCreatedSocketParam})
    public void setServerOnlineThread(Bundle bundle) throws IOException, SocketExceptions {
        System.out.println("Server running");
        ServerCreatedSocket serverCreatedSocket = (ServerCreatedSocket) bundle.getArguments(serverCreatedSocketParam);
        serverCreatedSocket.initSocket();
        serverCreatedSocket.runSocket();
    }
}
