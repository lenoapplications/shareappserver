package server.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.pc_controller_threads.event_handler_threads;

import pc_controller.controller.PcController;
import server.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.pc_controller_threads.event_handler_threads.events_impl.MouseEvents;
import server.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.pc_controller_threads.event_handler_threads.events_tools.screen_handler.ScreenHandler;
import thread_watcher.models.abstractions.user_method.UserMethod;
import thread_watcher.models.annotations.ThreadMethod;
import thread_watcher.user_parts.thread_bundle.Bundle;

import java.util.concurrent.ConcurrentLinkedQueue;


public class EventHandlerThread extends UserMethod {
    private final String events = "Evnts";
    private final String pcController = "pcCntrll";
    private final String screenSize = "scrnSz";


    private final char MOUSE = 'M';
    private final char CLICK = 'C';
    private final char LONG_CLICK = 'L';
    private final char DOUBLE_CLICK = 'W';
    private final char MOVE = 'M';
    private final char DRAG = 'D';




    private final MouseEvents mouseEvents = new MouseEvents();
    @ThreadMethod(paramNames = {pcController,events,screenSize})
    public void handleEvents(Bundle bundle){
        PcController pc = (PcController) bundle.getArguments(pcController);
        ConcurrentLinkedQueue<String> eventList = (ConcurrentLinkedQueue<String>) bundle.getArguments(events);
        String mobileScreenSize = (String) bundle.getArguments(screenSize);
        handleEvents(pc,eventList,mobileScreenSize.split("[|]"));

    }
    private void handleEvents(PcController pc,ConcurrentLinkedQueue<String> events,String[] mobileScreenSize){
        setupScreenDifferenceAndScreenHandler(pc.getConfigurations().screenConfiguration.SCREEN_WIDTH,pc.getConfigurations().screenConfiguration.SCREEN_HEIGHT,mobileScreenSize);

        while(true){
            if (events.size() > 0){
                checkEvent(pc,events.poll().substring(1));
            }
        }
    }
    private void setupScreenDifferenceAndScreenHandler(int SCREEN_WIDTH, int SCREEN_HEIGHT, String[] mobileScreenSize){
        float xDifference = SCREEN_WIDTH / Float.parseFloat(mobileScreenSize[0]);
        float yDifference = SCREEN_HEIGHT / Float.parseFloat(mobileScreenSize[1]);
        mouseEvents.initScreenDifference(xDifference,yDifference);
    }

    private void checkEvent(PcController pcController,String event){
        switch (event.charAt(0)){
            case MOUSE:checkMouseEvent(pcController,event);break;
        }
    }

    private void checkMouseEvent(PcController pcController,String event){
        System.out.println(event);
        switch (event.charAt(1)){
            case CLICK: mouseEvents.singleClick(pcController,event);break;
            case DOUBLE_CLICK: mouseEvents.doubleClick(pcController,event);break;
            case LONG_CLICK: mouseEvents.rightClick(pcController,event);break;
            case MOVE: mouseEvents.moveMouse(pcController,event);break;
        }
    }

}
