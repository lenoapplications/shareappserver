package server.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.pc_controller_threads.event_handler_threads.events_impl;

import pc_controller.controller.PcController;
import pc_controller.input_objects.awt_robot.input_devices.common_device_actions.action_master.ActionDeviceMaster;
import pc_controller.models.interfaces.input_objects.awt_robot.device_runner.DeviceRunnerModel;
import server.models.components.thread_components.pc_controller_threads.EventsHelper;

public class MouseEvents extends EventsHelper {


    public MouseEvents(){
    }

    public void singleClick(PcController pcController, String clickEvent){
        int xCoordinate = caluclateXDifference(parseXCoordinateFromString(clickEvent));
        int yCoordinate = caluclateYDifference(parseYCoordinateFromString(clickEvent));

        pcController.getDeviceMaster().activateDevice(new DeviceRunnerModel() {
            @Override
            public void runDevice(ActionDeviceMaster actionDeviceMaster) {
                actionDeviceMaster.getMouseCommonActions().moveAndClickLeft(xCoordinate,yCoordinate);
            }
        });
    }
    public void doubleClick(PcController pcController, String clickEvent){
        int xCoordinate = caluclateXDifference(parseXCoordinateFromString(clickEvent));
        int yCoordinate = caluclateYDifference(parseYCoordinateFromString(clickEvent));

        pcController.getDeviceMaster().activateDevice(new DeviceRunnerModel() {
            @Override
            public void runDevice(ActionDeviceMaster actionDeviceMaster) {
                actionDeviceMaster.getMouseCommonActions().moveAndDoubleClickLeft(xCoordinate,yCoordinate);
            }
        });
    }
    public void rightClick(PcController pcController,String clickEvent){
        int xCoordinate = caluclateXDifference(parseXCoordinateFromString(clickEvent));
        int yCoordinate = caluclateYDifference(parseYCoordinateFromString(clickEvent));

        pcController.getDeviceMaster().activateDevice(new DeviceRunnerModel() {
            @Override
            public void runDevice(ActionDeviceMaster actionDeviceMaster) {
                actionDeviceMaster.getMouseCommonActions().moveAndClickRightButton(xCoordinate,yCoordinate);
            }
        });
    }

    public void moveMouse(PcController pcController,String dragEvent){
        int xCoordinate = caluclateXDifference(parseXCoordinateFromString(dragEvent));
        int yCoordinate = caluclateYDifference(parseYCoordinateFromString(dragEvent));

        pcController.getDeviceMaster().activateDevice(new DeviceRunnerModel() {
            @Override
            public void runDevice(ActionDeviceMaster actionDeviceMaster) {
                actionDeviceMaster.getMouseCommonActions().moveMouse(xCoordinate,yCoordinate);
            }
        });
    }
}
