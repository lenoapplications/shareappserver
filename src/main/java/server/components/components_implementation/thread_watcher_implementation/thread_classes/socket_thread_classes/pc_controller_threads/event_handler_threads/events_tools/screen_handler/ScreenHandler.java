package server.components.components_implementation.thread_watcher_implementation.thread_classes.socket_thread_classes.pc_controller_threads.event_handler_threads.events_tools.screen_handler;



public class ScreenHandler {
    private int x;
    private int y;
    private int width;
    private int height;
    
    
    public ScreenHandler(int x,int y,int width,int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    
    
    public void moveX(int steps){
        this.x +=steps;
    }
    public void moveY(int steps){
        this.y +=steps;
    }
    
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
